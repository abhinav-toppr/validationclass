import json
import logging
from django.views.generic.base import View
from django.http import HttpResponse
from django.utils.decorators import classonlymethod
from functools import update_wrapper
import yaml
import os
from django.conf import settings

log = logging.getLogger('toppr_ask')
                                                                                   
class ApiBase(View):
    def __init__(self, **kwargs):
        self.status_code = None
        self.message = None
        self.success = None

    @classonlymethod
    def as_api_view(cls, **initkwargs):
        """
        Main entry point for a request-response process.
        """
        # sanitize keyword arguments
        for key in initkwargs:
            if key in cls.http_method_names:
                raise TypeError("You tried to pass in the %s method name as a "
                                "keyword argument to %s(). Don't do that."
                                % (key, cls.__name__))
            if not hasattr(cls, key):
                raise TypeError("%s() received an invalid keyword %r" % (
                    cls.__name__, key))
        
        def view(request, *args, **kwargs):
            self = None

            if not self:
                self = cls(**initkwargs)
            if hasattr(self, 'get') and not hasattr(self, 'head'):
                self.head = self.get
            self.request = request
            self.args = args
            self.kwargs = kwargs
            return self.dispatch(request, *args, **kwargs)

        # take name and docstring from class
        update_wrapper(view, cls, updated=())

        # and possible attributes set by decorators
        # like csrf_exempt from dispatch
        update_wrapper(view, cls.dispatch, assigned=())
        return view
                                                                                   
    def get(self, request, *args, **kwargs):
        return self.render_response()
                                                                                   
    def post(self, request, *args, **kwargs):
        return self.render_response()

    def set_bad_req(self, msg):
        self.success = False
        self.status_code = 400
        self.message = msg

    def set_fatal_error(self, msg):
        self.success = False
        self.status_code = 500
        self.message = msg

    def resp_meta(self):
        self.status_code = self.status_code if self.status_code else 200
        self.message = self.message if self.message else 'Request successful'
        resp = {
            'success': self.success,
            'status': self.status_code,
            'message': self.message
        }
        return resp
    
    def render_response(self, **resp_args):
        self.success = True
        resp = {}
        try:
            ctxt = { 'data': None }
            data = self.get_or_create()
            resp_meta = self.resp_meta()
            ctxt['data'] = data
            resp.update(resp_meta)
            resp.update(ctxt)
            return HttpResponse(json.dumps(resp), content_type="application/json", status=self.status_code)
        except Exception as e:
            log.exception(e)
            resp = {
                'success': False,
                'status': 500,
                'message': 'Something went wrong'
            }
            return HttpResponse(json.dumps(resp), content_type="application/json", status=500)
    def converting_types_from_str(self,input_data_type,value):
        if input_data_type == 'int':
            return int(value)
        elif input_data_type == 'str':
            return str(value)
        elif input_data_type == 'list':
            return list(value)
    
    def validate(self,request,**kwargs):
        print('kwargs: ',kwargs)
        APP_CONFIG = settings.APP_CONFIG
        mandatory = APP_CONFIG[kwargs['api_name']]['mandatory']
        optional = APP_CONFIG[kwargs['api_name']]['optional']
        mandatory_params=dict()
        optional_params=dict()
        try:
            for key in mandatory:
                mandatory_params[key]=kwargs.get(key)
                if mandatory_params[key] is None:
                    self.set_bad_req("Missing idetifier "+key)
                    
            for key in optional:
                optional_params[key]=request.GET.get(key,None)
        except:
            self.set_fatal_error("Something went wrong")
         
        for key in mandatory:
            try:
                 mandatory_params[key] = self.converting_types_from_str(mandatory[key],mandatory_params[key])
            except:
                self.set_bad_req("Invalid "+key)
        if 'method'  in kwargs and kwargs['method']=='post':
            temp_dict = APP_CONFIG[kwargs['api_name']][kwargs['method']]['body']
            body_params = dict()
            data = json.loads(self.request.body)
            for key in temp_dict:
                body_params[key] = data.get(key,None)
                if body_params[key] is None:
                    self.set_bad_req("Body does not have "+key)
            for key in body_params:
                try:
                    body_params[key] = self.converting_types_from_str(temp_dict[key],body_params[key])
                except:
                    self.set_bad_req("Invalid "+key)
            mandatory_params.update(body_params)
        
        mandatory_params.update(optional_params)
        
        print(mandatory_params)
        return (mandatory_params)

            

                
                

        
        
                
        
                 


           
