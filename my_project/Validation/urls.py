from django.conf.urls import url
from .views import TestingValidation , TestingValidationPost
from django.views.decorators.csrf import csrf_exempt
urlpatterns=[
    url(r'^validation/(?P<user_id>\w+)/$',TestingValidation.as_api_view()),
    url(r'^validation/(?P<user_id>\w+)/submit/$',csrf_exempt(TestingValidationPost.as_api_view())),
    
]